#!/usr/bin/perl

package OTParser;

use strict;
use warnings;
use Config::Simple;

our $templatesDir;
our %defaultVars;

sub setTemplatesDir($) {
	$templatesDir = shift;
}

sub parseTemplate {
	my $code = shift;
	my %vars = (%defaultVars, %{shift()});
	while (my ($key, $value) = each %vars) {
		if ($key !~ /^[a-zA-Z]+$/) {
			warn "Incorrect variable '$key', skipping";
			next;
		}
		if ($code =~ /^(\s)*\@\@\@$key\@\@\@/mi) {
			my $tmp = $1;
			$value =~ s/\n/\n$tmp/g;
		}
		$code =~ s/\@\@\@$key\@\@\@/$value/gi;
	}
	while ($code =~ /\@\@\@([a-z]+)\(\)\@\@\@/i) {
		unless (-e "$templatesDir/$1.html") {
			warn "Incorrect template '$1', skipping";
			$code =~ s/\@\@\@$1\(\)\@\@\@//gi;
			next;
		}
		open my ($innerTemplateFile), "$templatesDir/$1.html";
		local $/ = undef;
		my $innerTemplate = <$innerTemplateFile>;
		my $innerTemplateParsed = parseTemplate ($innerTemplate, \%vars);
		$code =~ s/\@\@\@$1\(\)\@\@\@/$innerTemplateParsed/gi;
	}
	while ($code =~ /\@\@\@([a-z]+)\@\@\@/) {
		warn "Unknown variable '$1', skipping";
		$code =~ s/\@\@\@$1\@\@\@//gi;
	}
	return $code;
}

sub addDefaultVars(\%) {
	%defaultVars = (%defaultVars, %{shift()});
}

sub addFromConfig($) {
	my %newVars;
	Config::Simple->import_from(shift, \%newVars);
	addDefaultVars(%newVars);
}

1;

