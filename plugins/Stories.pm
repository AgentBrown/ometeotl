#!/usr/bin/perl

package plugins::Stories;

push @INC, '..';

use strict;
use warnings;
require OTParser;

our $postsCount = 15;

our %OMETEOTL_MODULE = (
	name => 'Stories',
	version => '0.1',
	description => 'Provides simple interface for posts.',
	provides => {stories => \&getStories},
	requireTemplates => ['post', 'image', 'joiner'],
);

sub init() {
	OTParser::addFromConfig('config.cfg');
}

sub getStories($$) {
	my $rootDir = shift;
	my $templatesDir = shift;
	if (! -d "$rootDir/stories") {
		return '';
	}
	my @stories = glob("$rootDir/stories/*");
	my $remaining = $postsCount;
	open STORY_TEMPLATE, "$templatesDir/post.html";
	open IMAGE_TEMPLATE, "$templatesDir/image.html";
	local $/ = undef;
	my $storyTemplate = <STORY_TEMPLATE>;
	my $imageTemplate = <IMAGE_TEMPLATE>;
	close STORY_TEMPLATE;
	close IMAGE_TEMPLATE;
	my @readyStories;
	foreach my $story (@stories) {
		my $storyNum = $story;
		$storyNum =~ s/.*([0-9]+)$/$1/;
		open STORYFILE, $story;
		local $/ = "\n";
		my $timestamp = <STORYFILE>;
		my ($sec, $min, $hour, $day, $mon, $year) = localtime $timestamp;
		my $timeStr = "$day.$mon.$year $hour:$min:$sec";
		my $imageUrl = <STORYFILE>;
		my $nickname = <STORYFILE>;
		chomp $imageUrl;
		chomp $nickname;
		my $image = ($imageUrl ne '' ? OTParser::parseTemplate($imageTemplate, {url => $imageUrl}) : '');
		local $/ = undef;
		my $text = <STORYFILE>;
		my $readyStory = OTParser::parseTemplate ($storyTemplate,
		                                          {
		                                          	number => $storyNum,
		                                          	text => $text,
		                                          	nickname => $nickname,
		                                          	datestring => $timeStr,
		                                          	image => $image,
		                                          });
		push @readyStories, $readyStory;
	}
	open JOINER_TEMPLATE, "$templatesDir/joiner.html";
	local $/ = undef;
	my $joinerTemplate = <JOINER_TEMPLATE>;
	close JOINER_TEMPLATE;
	return join OTParser::parseTemplate($joinerTemplate, {}), @readyStories;
}

sub addStory($\%) {
	my $rootDir = shift;
	my %storyHash = %{ shift() };
	if (!(exists $storyHash{'text'})) {
		warn 'Don\'t want to post story without a text, skipping';
		return;
	}
	my $text = $storyHash{'text'};
	my $timestamp = time;
	my $nickname = 'Anonymous';
	if (exists $storyHash{'nickname'}) {
		$nickname = $storyHash{'nickname'};
	}
	my $image = '';
	if (exists $storyHash{'image'}) {
		$image = $storyHash{'image'};
	}
	my @files = glob("$rootDir/stories/*");	
	my $max = -1;
	foreach my $file (@files) {
		my $num = $file;
		$num =~ s/.*?([0-9]+)$/$1/;
		next unless ($num =~ /.*[0-9]+$/);
		if ($num > $max) {
			$max = $num;
		}
	}
	my $filename = $max + 1;
	open STORYFILE, '>', "$rootDir/stories/$filename";
	print STORYFILE "$timestamp\n$image\n$nickname\n$text";
	close STORYFILE;
}

1;

