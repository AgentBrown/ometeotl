#!/usr/bin/perl

package plugins::Blank;

use strict;
use warnings;

our %OMETEOTL_MODULE = (
	name => 'Blank',
	version => '1.0',
	description => 'Does nothing.',
	provides => {},
);

sub init() {}

1;

